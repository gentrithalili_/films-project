import axios from "axios";

const API_URL = "http://localhost:4000/api";

const API = {
  movies: {
    getMovies: () =>
      axios.get(`${API_URL}/authfilms`).then((response) => response.data.films),

    update: (movie) =>
      axios
        .put(`${API_URL}/authfilms/${movie._id}`, { film: movie })
        .then((response) => response.data.film),

    create: (movie) =>
      axios
        .post(`${API_URL}/authfilms`, { film: movie })
        .then((response) => response.data.film),

    delete: (movieId) => axios.delete(`${API_URL}/authfilms/${movieId}`),
  },
  users: {
    login: (credentials) =>
      axios
        .post(`${API_URL}/auth`, {
          credentials,
        })
        .then((response) => response.data.token),
  },
};

export default API;
