import Axios from "axios";

export default function setAuthorizationToken(token) {
  if (!token) {
    delete Axios.defaults.headers.Authorization;
    return;
  }

  Axios.defaults.headers.Authorization = `Bearer ${token}`;
}
