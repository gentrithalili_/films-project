import React from "react";
import { useAuthContext } from "../context/AuthContext";

const Home = () => {
  const authContext = useAuthContext();
  return (
    <div className="Home" style={{ marginTop: 20, textAlign: "center" }}>
      <h1>Home Component</h1>
      {authContext.isAdmin ? "isadmin" : "isuser"}
    </div>
  );
};

export default Home;
