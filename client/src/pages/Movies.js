import React from "react";
import { Switch, Route } from "react-router-dom";
import MovieContext from "../context/MovieContext";
import MovieList from "../components/movie/MovieList";
import AddMovie from "./AddMovie";
import EditMovie from "./EditMovie";
import Message from "../components/shared/Message/Message";
import API from "../api/Api";
import AdminRoute from "../components/auth/AdminRoute";

class Movies extends React.Component {
  state = {
    loading: true,
    movies: [],
  };

  componentDidMount() {
    API.movies
      .getMovies()
      .then((movies) => {
        this.setState({
          ...this.state,
          movies: movies,
        });
      })
      .catch((error) => {
        console.log("error", error);
      })
      .finally(() => {
        this.setState({ ...this.state, loading: false });
      });
  }

  toggleFeatured = (id) => {
    const movie = this.state.movies.find((movie) => movie._id === id);

    if (!movie) {
      return;
    }

    API.movies
      .update({ ...movie, featured: !movie.featured })
      .then((updatedMovie) => {
        this.setState({
          ...this.state,
          movies: this.state.movies.map((movie) =>
            movie._id === updatedMovie._id ? updatedMovie : movie
          ),
        });
      });
  };

  deleteMovie = (id) => {
    API.movies.delete(id).then(() => {
      this.setState({
        ...this.state,
        movies: this.state.movies.filter((movie) => movie._id !== id),
      });
    });
  };

  addMovie = (movieData) => {
    return API.movies.create(movieData).then((createdMovie) => {
      this.setState({
        ...this.state,
        movies: [...this.state.movies, createdMovie],
      });
    });
  };

  updateMovie = (movieData) => {
    return API.movies.update(movieData).then((updatedMovie) => {
      this.setState({
        ...this.state,
        movies: this.state.movies.map((movie) =>
          movie._id === updatedMovie._id ? updatedMovie : movie
        ),
      });
    });
  };

  getMovieById = (id) => this.state.movies.find((movie) => movie._id === id);

  render() {
    return (
      <MovieContext.Provider
        value={{
          addMovie: this.addMovie,
          updateMovie: this.updateMovie,
          deleteMovie: this.deleteMovie,
          toggleFeatured: this.toggleFeatured,
          getMovieById: this.getMovieById,
        }}
      >
        <div className="Movies" style={{ marginTop: 20 }}>
          {this.state.loading ? (
            <Message icon="sync" text="Loading movies..." />
          ) : (
            <Switch>
              <Route
                exact={true}
                path="/movies"
                render={(props) => {
                  return (
                    <MovieList
                      {...props}
                      loading={this.state.loading}
                      movies={this.state.movies}
                    />
                  );
                }}
              />

              <AdminRoute path="/movies/new" component={AddMovie} />
              <AdminRoute path="/movies/edit/:movieId" component={EditMovie} />
            </Switch>
          )}
        </div>
      </MovieContext.Provider>
    );
  }
}

export default Movies;
