import React from "react";
import LoginForm from "../components/auth/LoginForm";
import API from "./../api/Api";
import { useAuthContext } from "../context/AuthContext";

const Login = (props) => {
  const authContext = useAuthContext();

  const handleOnSubmit = (data) => {
    API.users.login(data).then((token) => {
      authContext.login(token);
      props.history.push("/");
    });
  };

  return (
    <div className="Login">
      <LoginForm onSubmit={handleOnSubmit} />
    </div>
  );
};

export default Login;
