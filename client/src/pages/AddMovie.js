import React from "react";
import MovieForm from "../components/movie/MovieForm";
import { useMovieContext } from "../context/MovieContext";

const AddMovie = (props) => {
  const movieContext = useMovieContext();
  return (
    <div className="AddMovie" style={{ marginTop: 20 }}>
      <MovieForm
        onSubmit={(movieData) => {
          return movieContext.addMovie(movieData);
        }}
      />
    </div>
  );
};

export default AddMovie;
