import React from "react";
import MovieForm from "../components/movie/MovieForm";
import { useMovieContext } from "../context/MovieContext";
import Message from "../components/shared/Message/Message";

const EditMovie = (props) => {
  const { movieId } = props.match.params;
  const movieContext = useMovieContext();
  const movie = movieContext.getMovieById(movieId);

  return (
    <div className="EditMovie" style={{ marginTop: 20 }}>
      {!movie ? (
        <Message text="Movie not found !" />
      ) : (
        <>
          <h1>Edit Movie</h1>
          <MovieForm
            movie={movie}
            onSubmit={(movieData) => {
              return movieContext.updateMovie(movieData);
            }}
          />
        </>
      )}
    </div>
  );
};

export default EditMovie;
