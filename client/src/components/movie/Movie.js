import React, { useState } from "react";
import { useMovieContext } from "../../context/MovieContext";
import Favorite from "./Favorite";
import { Link } from "react-router-dom";
import { useAuthContext } from "../../context/AuthContext";

const Movie = (props) => {
  const { movie } = props;
  const authContext = useAuthContext();
  const { deleteMovie } = useMovieContext();

  const [showConfirm, setShowConfirm] = useState(false);

  const handleDelete = () => deleteMovie(movie._id);

  const showConfirmation = () => setShowConfirm(true);

  const hideConfirmation = () => setShowConfirm(false);

  const userActions = !authContext.isAdmin && (
    <div className="ui two buttons">
      <span className="ui green basic button">Add to cart</span>
    </div>
  );

  const adminActions = authContext.isAdmin && (
    <>
      {showConfirm ? (
        <div className="ui two buttons">
          <span className="ui red basic button" onClick={handleDelete}>
            <i className="ui icon check">YES</i>
          </span>
          <span className="ui grey basic button" onClick={hideConfirmation}>
            <i className="ui icon close">NO</i>
          </span>
        </div>
      ) : (
        <div className="ui two buttons">
          <span className="ui green basic button">
            <Link to={`/movies/edit/${movie._id}`}>
              <i className="ui icon edit"></i>
            </Link>
          </span>
          <span className="ui red basic button" onClick={showConfirmation}>
            <i className="ui icon trash"></i>
          </span>
        </div>
      )}
    </>
  );

  return (
    <div className="Movie ui card">
      <div className="image">
        <span className="ui green label ribbon">${movie.price} </span>
        {authContext.isAdmin && (
          <Favorite movieId={movie._id} isFavorite={movie.featured} />
        )}
        <img src={movie.img} alt={movie.title} />
      </div>

      <div className="content">
        <a href="#" className="header">
          {movie.title}
        </a>
        <div className="meta">
          <i className="icon users"></i> {movie.director}
          <span className="right floated">
            <i className="icon wait right"></i> {movie.duration} min
          </span>
        </div>
      </div>

      {authContext.isAuthenticated && (
        <div className="extra content">
          {userActions}
          {adminActions}
        </div>
      )}
    </div>
  );
};

export default Movie;
