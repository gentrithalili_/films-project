import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import Input from "../shared/Input/Input";

const initialFormData = {
  title: "",
  description: "",
  img: "http://via.placeholder.com/250x250",
  director: "",
  duration: "",
  price: "",
  featured: true,
};

const MovieForm = (props) => {
  const history = useHistory();

  const [formData, setFormData] = useState(props.movie || initialFormData);
  const [errors, setErrors] = useState({});

  const handleOnChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
  };

  const handleCheckboxChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.checked,
    });
  };

  const handleOnSubmit = (e) => {
    e.preventDefault();

    props
      .onSubmit(formData)
      .then(() => {
        setErrors({});
        history.push("/movies");
      })
      .catch((errors) => {
        setErrors(errors.response.data.errors);
      });
  };

  return (
    <div className="MovieForm">
      <div className="ui stackable grid">
        <form className="ui form" onSubmit={handleOnSubmit}>
          <div className="ui  grid">
            <div className="twelve wide column">
              <Input
                label="Film title"
                type="text"
                name="title"
                id="title"
                error={errors?.title}
                placeholder="film title"
                value={formData.title}
                onChange={handleOnChange}
              />

              <div className={`field ${errors?.description ? "error" : ""}`}>
                <label>Film description</label>
                <textarea
                  value={formData.description}
                  onChange={handleOnChange}
                  name="description"
                  id="description"
                  placeholder="film description"
                />
                {errors?.description && (
                  <div style={{ color: "#9a3f38" }}>{errors?.description}</div>
                )}
              </div>
            </div>

            <div className="four wide column">
              <img src={formData.img} className="ui image" />
            </div>

            <Input
              className="twelve wide column"
              label="Image"
              type="text"
              name="img"
              id="img"
              error={errors?.img}
              placeholder="img"
              value={formData.img}
              onChange={handleOnChange}
            />

            <Input
              className="six wide column"
              label="Director"
              type="text"
              name="director"
              id="director"
              error={errors?.director}
              placeholder="film director"
              value={formData.director}
              onChange={handleOnChange}
            />

            <Input
              className="six wide column"
              label="Duration"
              type="number"
              name="duration"
              id="duration"
              error={errors?.duration}
              placeholder="Duration"
              value={formData.duration}
              onChange={handleOnChange}
            />

            <Input
              className="six wide column"
              label="Price"
              type="number"
              name="price"
              id="price"
              error={errors?.price}
              placeholder="price"
              value={formData.price}
              onChange={handleOnChange}
            />

            <div className="six wide column inline field">
              <label htmlFor="featured">Featured</label>
              <input
                type="checkbox"
                name="featured"
                id="featured"
                checked={formData.featured}
                onChange={handleCheckboxChange}
              />
            </div>
          </div>

          <div className="ui fluid buttons mt-2">
            <button className="ui button primary" type="submit">
              Save
            </button>
            <div className="or" />
            <Link to="/movies" className="ui button">
              Hide form
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
};

export default MovieForm;
