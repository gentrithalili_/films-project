import React from "react";
import Movie from "./Movie";
import Message from "../shared/Message/Message";

export const MovieList = (props) => {
  const { loading, movies } = props;

  return (
    <div className="ui container">
      <div className="ui four cards">
        {loading ? (
          <Message icon="sync" text="Loading movies..." />
        ) : movies.length === 0 ? (
          <Message text="There are no data " />
        ) : (
          movies.map((movie) => {
            return <Movie key={movie._id} movie={movie} />;
          })
        )}
      </div>
    </div>
  );
};

export default MovieList;
