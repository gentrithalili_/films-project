import React from "react";
import { NavLink } from "react-router-dom";
import { useAuthContext } from "../../context/AuthContext";
import "./Header.css";

const Header = () => {
  const authContext = useAuthContext();

  return (
    <header className="Header mb-3">
      {authContext.isAuthenticated && (
        <div className="Header-user-info">
          <small>user: {authContext.user.email}</small>
          <small>role: {authContext.user.role}</small>
        </div>
      )}

      <div className="ui secondary pointing menu">
        <NavLink exact to="/" className="item">
          Home
        </NavLink>

        <NavLink exact to="/movies" className="item">
          Movies
        </NavLink>

        {authContext.isAdmin && (
          <NavLink to="/movies/new" className="item">
            <i className="icon plus" />
            Add new movie
          </NavLink>
        )}

        <div className="right menu">
          {authContext.isAuthenticated ? (
            <span
              role="button"
              className="ui item warning"
              onClick={() => authContext.logout()}
            >
              Logout
            </span>
          ) : (
            <>
              <NavLink exact to="/login" className="item">
                Login
              </NavLink>
              <NavLink to="/register" className="item">
                Register
              </NavLink>
            </>
          )}
        </div>
      </div>
    </header>
  );
};

export default Header;
