import React from "react";
import { Redirect, Route } from "react-router-dom";
import { useAuthContext } from "../../context/AuthContext";

const AdminRoute = (props) => {
  const authContext = useAuthContext();

  if (!authContext.isAuthenticated) {
    return <Redirect to="/login" />;
  }

  if (!authContext.isAdmin) {
    return <Redirect to="/" />;
  }

  return <Route {...props} />;
};

export default AdminRoute;
