import React, { useState } from "react";
import { Link } from "react-router-dom";
import Input from "../shared/Input/Input";

const LoginForm = (props) => {
  const [errors, setErrors] = useState({});
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });

  const validate = () => {
    const errors = {};

    if (!formData.email) {
      errors.email = "Email is required!";
    }
    if (!formData.password) {
      errors.password = "Password is required!";
    }

    return errors;
  };

  const handleOnChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
  };

  const handleOnSubmit = (event) => {
    event.preventDefault();
    const errors = validate();

    if (Object.keys(errors).length !== 0) {
      setErrors(errors);
      return;
    }

    props.onSubmit(formData);
  };

  return (
    <div className="LoginForm">
      <form className="ui form" onSubmit={handleOnSubmit}>
        <div className="ui grid">
          <div className="seven wide column">
            <Input
              type="email"
              label="Email"
              name="email"
              error={errors?.email}
              value={formData.email}
              onChange={handleOnChange}
            />

            <Input
              label="Password"
              type="password"
              name="password"
              error={errors?.password}
              value={formData.password}
              onChange={handleOnChange}
            />

            <div className="ui fluid buttons">
              <button className="ui button primary" type="submit">
                Login
              </button>
              <div className="or" />
              <Link to="/register" className="ui button">
                Register
              </Link>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
};

export default LoginForm;
