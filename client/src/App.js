import React from "react";
import { Switch, Route } from "react-router-dom";
import Header from "./components/Header/Header";
import Home from "./pages/Home";
import Movies from "./pages/Movies";
import Login from "./pages/Login";
import Register from "./pages/Register";
import PageNotFound from "./pages/PageNotFound";
import AuthContext from "./context/AuthContext";

import jwtDecode from "jwt-decode";
import setAuthorizationToken from "./utils/setAuthorizationToken";
import PublicRoute from "./components/auth/PublicRoute";

const localStroageKey = "films-project-gr3";

class App extends React.Component {
  state = {
    user: null,
  };

  componentDidMount() {
    const token = localStorage.getItem(localStroageKey);
    if (token) {
      const { user } = jwtDecode(token);
      this.setState({
        user: {
          token: token,
          ...user,
        },
      });
      setAuthorizationToken(token);
    }
  }

  login = (token) => {
    const { user } = jwtDecode(token);
    this.setState({
      user: {
        token: token,
        ...user,
      },
    });
    localStorage.setItem(localStroageKey, token);
    setAuthorizationToken(token);
  };

  logout = () => {
    this.setState({
      user: null,
    });
    localStorage.removeItem(localStroageKey);
    setAuthorizationToken(undefined);
  };

  render() {
    return (
      <AuthContext.Provider
        value={{
          isAuthenticated: Boolean(this.state.user),
          isAdmin: this.state.user && this.state.user.role === "admin",
          user: this.state.user,
          login: this.login,
          logout: this.logout,
        }}
      >
        <div className="App ui container">
          <Header />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/movies" component={Movies} />
            <PublicRoute path="/login" component={Login} />
            <PublicRoute path="/register" component={Register} />

            <Route component={PageNotFound} />
          </Switch>
        </div>
      </AuthContext.Provider>
    );
  }
}

export default App;
